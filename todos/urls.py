from django.urls import path
from todos.views import todo_list, show_todo_list, todo_list_create, todo_list_edit, todo_list_delete, todo_item_create, todo_item_edit


urlpatterns = [
    path('items/<int:id>/edit/', todo_item_edit, name="todo_item_edit"),
    path('items/create/', todo_item_create, name="todo_item_create"),
    path('<int:id>/delete/', todo_list_delete, name="todo_list_delete"),
    path('<int:id>/edit/', todo_list_edit, name="todo_list_edit"),
    path('create/', todo_list_create, name="todo_list_create"),
    path('<int:id>/', show_todo_list, name='todo_list_detail'),
    path('', todo_list, name='todo_list'),
]
